package rasterize;

import model.Point;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;

public class TrivialLineRasterizer extends LineRasterizer {

    public TrivialLineRasterizer(Raster raster) {
        super(raster);
    }
    public List<Point> pointsList = new ArrayList<>();

    public List<Point> returnLine(){return pointsList;}

    @Override
    public int rasterize(int x1, int y1, int x2, int y2, int color) {
//      triviální algoritmus

        //TODO plusy a mínusy algoritmu

        pointsList.clear();
        //System.out.println("Point list cleared");
        float k = (y2 - y1) / (float) (x2 - x1);
        float q = y1 - k * x1;

        if (Math.abs(y2 - y1) < Math.abs(x2 - x1)) {
            if (x2 < x1) {
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }

            for (int x = x1; x <= x2; x++) {
                float y = (k * x) + q;
                raster.setPixel(x, Math.round(y), color);
                pointsList.add(new Point(x, Math.round(y)));
            }
        } else {
            if (y2 < y1) {
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }

            for (int y = y1; y <= y2; y++) {
                float x = (y - q) / k;
                raster.setPixel((int) x, y, color);
                pointsList.add(new Point(x,y));
            }
        }

        return x1;
    }
}
