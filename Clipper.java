package rasterize;

import model.Edge;
import model.Line;
import model.Point;
import model.Polygon;

import java.util.ArrayList;
import java.util.List;

public class Clipper {


    private static boolean inside(Point p, Line line) {
        Point v1 = new Point(line.getX2() - line.getX1(), line.getY2() - line.getY1());

        Point pnt = new Point(v1.getY(), -v1.getX());

        Point v2 = new Point(p.getX() - line.getX1(), p.getY() - line.getY1());

        int i = (pnt.getX() * v2.getX() + pnt.getY() * v2.getY());

        return (i < 0.0D);
    }

    private static Point intersection(Point v1, Point v2, Line line) {
        Point v3 = new Point(line.getX1(), line.getY1());
        Point v4 = new Point(line.getX2(), line.getY2());
        int x = ((((v1.getX() * v2.getY()) - (v2.getX()) * v1.getY()) * (v3.getX() - v4.getX())) -
                (((v3.getX() * v4.getY()) - (v4.getX() * v3.getY())) * (v1.getX() - v2.getX()))) /
                (((v1.getX() - v2.getX()) * (v3.getY() - v4.getY())) - ((v1.getY() - v2.getY()) * (v3.getX() - v4.getX())));
        int y = ((((v1.getX() * v2.getY()) - (v2.getX() * v1.getY())) * (v3.getY() - v4.getY())) -
                (((v3.getX() * v4.getY()) - (v4.getX() * v3.getY())) * (v1.getY() - v2.getY()))) /
                (((v1.getX() - v2.getX()) * (v3.getY() - v4.getY())) - ((v1.getY() - v2.getY()) * (v3.getX() - v4.getX())));
        return new Point(x, y);
    }

    /**
     * Ořezání obecného polygonu jinám konvexním polygonem (clipPolygon)
     *
     * @param polygon     ořezávaný polygon
     * @param clipPolygon ořezávací polygon
     * @return seznam bodů ořezaného polygonu
     */

    public static List<Point> clip(List<Point> polygon, List<Point> clipPolygon) {
        List<Point> out = new ArrayList<>();

        List<Point> in = new ArrayList<>(polygon);
        Point p1 = clipPolygon.get(clipPolygon.size() - 1); // TODO vložit poslední clip point (z clipPolygon)

        List<Line> edges = new ArrayList<>();
        for (int i = 0; i < clipPolygon.size(); i++) {
            int x1 = clipPolygon.get(i).getX();
            int y1 = clipPolygon.get(i).getY();
            int idx = (i + 1) % clipPolygon.size();
            int x2 = clipPolygon.get(idx).getX();
            int y2 = clipPolygon.get(idx).getY();

            Line edge = new Line(x1, y1, x2, y2, 0xFFFFFF);
            edges.add(edge);
        }

        for (Line edge : edges) {


            //E = vytvořit hranu z bodů p1 a p2

            //Edge edge = new Edge(p1, p2);
            Point v1 = in.get(in.size() - 1);
            for (Point v2 : in) {
                // vytvořit hranu z bodů v1 a v2

//               Edge edgeP = new Edge(p1, p2);
                if (inside(v2, edge)) {
                    if (!inside(v1, edge)) {// TODO chybí vnitřní podmínka na edge not inside v1
                        Point intersection = intersection(v1, v2, edge);
                        out.add(intersection);
                    }

                    out.add(v2);

                } else { //TODO není else, ale else if e inside v1
                    if (inside(v1, edge)) {
                        Point intersection = intersection(v1, v2, edge);
                        out.add(intersection);
                    }
                }
                // TODO v1 = v2;
                v1 = v2;
            }
            //p1 = p2;
            //in = out;
        }
        return out;
    }


}

