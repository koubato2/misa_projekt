package control;

import fill.SeedFiller;
import model.Edge;
import model.Line;
import model.Point;
import rasterize.*;
import view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class Controller2D implements Controller {

    private final Panel panel;
    private Raster raster = null;


    private LineRasterizer trivialLineRasterizer;
    private SeedFiller seedFiller;
    private ScanLineRasterizer scanLine;

    private Clipper clipper;
    private Edge edge;
    private Point point;
    private Line line;

    List<Point> pointsList = new ArrayList<>();
    List<Point> pointsListOld = new ArrayList<>();
    List<Point> aroundPointOneList = new ArrayList<>();
    List<Point> pointsListClipping = new ArrayList<>();

    //List<Point> polygon = new ArrayList<Point>();
    //List<Point> clipPolygon = new ArrayList<Point>();
    List<Point> clippedPolygon = new ArrayList<Point>();

    private int x, y, x2, y2;
    private boolean isCleared = false;
    private boolean polygonExists = false;

    public Controller2D(Panel panel) {
        this.panel = panel;
        this.raster = panel.getRaster();
        initObjects(panel.getRaster());
        initListeners(panel);

    }


    private void initObjects(Raster raster) {
        trivialLineRasterizer = new TrivialLineRasterizer(raster);

        seedFiller = new SeedFiller(raster);
        scanLine = new ScanLineRasterizer(raster);

    }


    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (x == 0 || y == 0 || isCleared) {
                        x = e.getX();
                        y = e.getY();
                        aroundPointOneList.clear();

                        isCleared = false;
                        Point bod = new Point(x, y);
                        pointsList.add(bod);
                        //Point startPoint = new Point(x-25, y-25);
                        int area = 20; //nastavuj na sude
                        for (int i = 0; i < area; i++) {
                            for (int j = 0; j < area; j++) {
                                aroundPointOneList.add(i + j, new Point(x + i - area / 2, y + j - area / 2));
                                drawPixel(x + i - area / 2, y + j - area / 2);
                            }
                        }
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (SwingUtilities.isLeftMouseButton(e)) {
                    System.out.println("Mouse released polygon");
                    x2 = e.getX();
                    y2 = e.getY();
                    System.out.println(x2);
                    boolean isFound = false;
                    for (Point point : aroundPointOneList) {
                        if (point.x == x2 && point.y == y2) {
                            System.out.println("Is around true");
                            if (!pointsList.isEmpty()) {

                                x2 = pointsList.get(0).x;
                                y2 = pointsList.get(0).y;


                                raster.clear();
                                trivialLineRasterizer.rasterize(x, y, x2, y2, 0xFFFFFF);
                                redrawOldPoints();
                                redrawOldLines();
                                panel.repaint();

//                            Point bod = new Point(x2, y2);
//                            pointsList.add(bod);
                                if (pointsListOld.isEmpty()) pointsListOld.addAll(pointsList);
                                else pointsListClipping.addAll(pointsList);
                                //pointsListOld.add(new Point(x2,y2));
                                pointsList.clear();
                                isCleared = true;
                                polygonExists = true;
                                isFound = true;
                            }
                        }
                    }
//                if (aroundPointOneList.contains(new Point(x2,y2))){
//                    x2 = pointsList.get(0).x;
//                    y2 = pointsList.get(0).y;
//                    System.out.println("Is around true");
//                }
                    if (!isFound) {
                        drawPixel(x2, y2);
                        Point bod = new Point(x2, y2);
                        pointsList.add(bod);
                        x = x2;
                        y = y2;
                        panel.repaint();
                    }
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.isControlDown()) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        //TODO
                    } else if (SwingUtilities.isRightMouseButton(e)) {
                       System.out.println("Right button clicked");
//                        SeedFiller seedFiller = new SeedFiller(raster);
//                        seedFiller.setSeed(new Point(e.getX(), e.getY()));
//                        seedFiller.setFillColor(Color.GREEN.getRGB());
//                        seedFiller.fill();
                        panel.clear();
                        redrawOldLines();
                        panel.repaint();
                        scanLine.fill();
                        redrawOldPoints();
                        panel.repaint();
                    }
                }
            }
        });

        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    //System.out.println("Mouse dragged");
                    raster.clear();
                    trivialLineRasterizer.rasterize(x, y, e.getX(), e.getY(), 0xFFFFFF);
                    redrawOldPoints();
                    redrawOldLines();
                    panel.repaint();
                }
            }
        });

        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                // na klávesu C vymazat plátno
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    pointsList.clear();
                    pointsListOld.clear();
                    raster.clear();
                    panel.repaint();
                    panel.clear();
                    isCleared = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_V) {
                    System.out.println("Key event V");
                    boolean notSame = false;
                   // polygon = new ArrayList<Point>(pointsListOld);
                   // clipPolygon = new ArrayList<Point>(pointsList);

                    clippedPolygon = Clipper.clip(pointsListOld, pointsListClipping);
        
                    //clippedPolygon.add(new Point(x, y));
                    clippedPolygon.removeAll(pointsListOld);

//                    for (int i = 0; i < clippedPolygon.size(); i++) {
//                        drawPixelCut(clippedPolygon.get(i).getX(), clippedPolygon.get(i).getY());
//                    }

                    for (int i = 0; i < clippedPolygon.size()-1; i++) {

                         trivialLineRasterizer.rasterize(clippedPolygon.get(i).getX(),clippedPolygon.get(i).getY(),
                                clippedPolygon.get(i+1).getX(),clippedPolygon.get(i+1).getY(), Color.WHITE.getRGB());
                        List<Point> newLine = trivialLineRasterizer.returnLine();
                        for(Point point : newLine){
                            if (raster.getPixel(point.getX(),point.getY()) != Color.WHITE.getRGB()){
                                notSame = true;
                            }
                        }

                        if(!notSame){
                            trivialLineRasterizer.rasterize(clippedPolygon.get(i).getX(),clippedPolygon.get(i).getY(),
                                    clippedPolygon.get(i+1).getX(),clippedPolygon.get(i+1).getY(), Color.RED.getRGB());
                            notSame = false;
                        }

                        panel.repaint();
                    }
                    for (int i = clippedPolygon.size()-1; i >= 1; i--) {

                        trivialLineRasterizer.rasterize(clippedPolygon.get(i).getX(),clippedPolygon.get(i).getY(),
                                clippedPolygon.get(i-1).getX(),clippedPolygon.get(i-1).getY(), Color.WHITE.getRGB());
                        List<Point> newLine = trivialLineRasterizer.returnLine();
                        for(Point point : newLine){
                            if (raster.getPixel(point.getX(),point.getY()) != Color.WHITE.getRGB()){
                                notSame = true;
                            }
                        }

                        if(!notSame){
                            trivialLineRasterizer.rasterize(clippedPolygon.get(i).getX(),clippedPolygon.get(i).getY(),
                                    clippedPolygon.get(i-1).getX(),clippedPolygon.get(i-1).getY(), Color.RED.getRGB());
                            notSame = false;
                        }
                        panel.repaint();
                    }

                   

                    //panel.clear();
//                    for (int i = 1; i < clippedPolygon.size(); i++) {
//                        trivialLineRasterizer.rasterize(clippedPolygon.get(i - 1).getX(), clippedPolygon.get(i - 1).getY(), clippedPolygon.get(i).getX(), clippedPolygon.get(i).getY(), Color.BLUE.getRGB());
//                    }
//                    trivialLineRasterizer.rasterize(clippedPolygon.get(0).getX(), clippedPolygon.get(0).getY(), clippedPolygon.get(clippedPolygon.size()-1).getX(), clippedPolygon.get(clippedPolygon.size()-1).getY(), Color.BLUE.getRGB());

                   // trivialLineRasterizer.rasterize(x, y, x2, y2, Color.BLUE.getRGB());

                    panel.repaint();



                }
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
            }
        });
    }

    private void update() {
//        panel.clear();
        //TODO
    }

    private void hardClear() {
        panel.clear();
    }

    void redrawOldPoints() {
        for (Point point : pointsList) {
            drawPixel(point.getX(), point.getY());
        }
        for (Point point : pointsListOld) {
            drawPixel(point.getX(), point.getY());
        }
    }

    void redrawOldLines() {
        for (int i = 1; i < pointsList.size(); i++) {
            trivialLineRasterizer.rasterize(pointsList.get(i - 1).getX(), pointsList.get(i - 1).getY(), pointsList.get(i).getX(), pointsList.get(i).getY(), 0xFFFFFF);
        }
        for (int i = 1; i < pointsListOld.size(); i++) {
            trivialLineRasterizer.rasterize(pointsListOld.get(i - 1).getX(), pointsListOld.get(i - 1).getY(), pointsListOld.get(i).getX(), pointsListOld.get(i).getY(), 0xFFFFFF);
        }
        if (polygonExists)
            trivialLineRasterizer.rasterize(pointsListOld.get(0).getX(), pointsListOld.get(0).getY(), pointsListOld.get(pointsListOld.size() - 1).getX(), pointsListOld.get(pointsListOld.size() - 1).getY(), 0xFFFFFF);
    }

    public void drawPixel(int a, int b) {
        int size = 2;
        int color = 0xFFFFFF;
        for (int i = -size; i <= size; i++)
            for (int j = -size; j <= size; j++)
                raster.setPixel(a + i, b + j, color);
    }
    public void drawPixelCut(int a, int b) {
        int size = 2;
        int color = 0xFFFFFF;
        for (int i = -size; i <= size; i++)
            for (int j = -size; j <= size; j++)
                raster.setPixel(a + i, b + j, Color.BLUE.getRGB());
    }

}
