import model.Point;
import rasterize.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

/**
 * trida pro kresleni na platno: vyuzita tridy RasterBufferedImage
 *
 * @author PGRF FIM UHK
 * @version 2020
 */

public class CanvasRasterBufferedImage {

    private JPanel panel;
    private RasterBufferedImage raster;
    private int x, y, x2, y2;
    private boolean isCleared = false;

    private LineRasterizerGraphics rasterizer;
    private FilledLineRasterizer filledLineRasterizer;
    private DottedLineRasterizer dottedLineRasterizer;
    private LineRasterizer lineRasterizer;

    List<model.Point> pointsList = new ArrayList<>();

    private model.Polygon polygon = new model.Polygon();

    private int program = 0;


    public CanvasRasterBufferedImage(int width, int height) {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());

        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferedImage(width, height);
        rasterizer = new LineRasterizerGraphics(raster);
        dottedLineRasterizer = new DottedLineRasterizer(raster) {
            @Override
            public void rasterize(int x1, int y1, int x2, int y2, int color) {

            }
        };

        filledLineRasterizer = new FilledLineRasterizer(raster) {
            @Override
            public void rasterize(int x1, int y1, int x2, int y2, int color) {

            }
        };

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (keyCode == KeyEvent.VK_C) {
                    // clear
                    pointsList.clear();
                    raster.clear();
                    panel.repaint();
                    isCleared = true;
                    program = 0;

                } else if (keyCode == KeyEvent.VK_P) {
                    //draw line
                    System.out.println("P pressed");
                    program = 1;
                    panel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent e) {
                            int size = 2;
                            int color = 0xFFFFFF;
                            // panel.removeMouseListener(this);
                            for (int i = -size; i <= size; i++)
                                for (int j = -size; j <= size; j++)
                                    raster.setPixel(e.getX() + i, e.getY() + j, color);

                            x = e.getX();
                            y = e.getY();

                            if (e.getButton() == MouseEvent.BUTTON1) {
                                System.out.println("Mode dotted line");
                                panel.addMouseMotionListener(new MouseMotionAdapter() {
                                    @Override
                                    public void mouseDragged(MouseEvent e) {
                                        System.out.println("Mouse dragged");
                                        raster.clear();
                                        dottedLineRasterizer.drawLine(x, y, e.getX(), e.getY());
                                        panel.repaint();
                                    }
                                });

                            } else if (e.getButton() == MouseEvent.BUTTON3) {
                                System.out.println("Mode filled line");
                                panel.addMouseMotionListener(new MouseMotionAdapter() {
                                    @Override
                                    public void mouseDragged(MouseEvent e) {
                                        System.out.println("Mouse dragged");
                                        raster.clear();
                                        filledLineRasterizer.drawLine(x, y, e.getX(), e.getY());
                                        panel.repaint();
                                    }
                                });
                            }
                        }
                    });

                } else if (keyCode == KeyEvent.VK_O) {
                    //draw polygon
                    program = 2;
                    panel.addMouseMotionListener(new MouseMotionAdapter() {
                        @Override
                        public void mouseDragged(MouseEvent e) {
                            System.out.println("Mouse dragged");
                            raster.clear();
                            filledLineRasterizer.drawLine(x, y, e.getX(), e.getY());
                            redrawOldPoints();
                            redrawOldLines();
                            panel.repaint();

                        }

                        void redrawOldPoints() {
                            for (Point point : pointsList) {
                                drawPixel(point.getX(), point.getY());
                            }
                        }

                        void redrawOldLines() {
                            for (int i = 1; i < pointsList.size(); i++) {
                                rasterizer.drawLine(pointsList.get(i - 1).getX(), pointsList.get(i - 1).getY(), pointsList.get(i).getX(), pointsList.get(i).getY());
                            }
                        }
                    });

                    panel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent e) {
                            super.mousePressed(e);
                            if (x == 0 || y == 0 || isCleared) {
                                x = e.getX();
                                y = e.getY();
                                isCleared = false;
                                Point bod = new Point(x, y);
                                pointsList.add(bod);
                            }
                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {
                            super.mouseReleased(e);
                            System.out.println("Mouse released polygon");
                            x2 = e.getX();
                            y2 = e.getY();
                            drawPixel(x2, y2);
                            Point bod = new Point(x2, y2);
                            pointsList.add(bod);
                            x = x2;
                            y = y2;
                            panel.repaint();
                        }
                    });

                } else if (keyCode == KeyEvent.VK_T) {
                    //draw triangle

                    program = 3;
                    panel.addMouseMotionListener(new MouseMotionAdapter() {
                        @Override
                        public void mouseDragged(MouseEvent e) {
                            System.out.println("Mouse dragged triangle");
                            raster.clear();
                            filledLineRasterizer.drawLine(x, y, e.getX(), e.getY());
                            panel.repaint();
                        }
                    });

                    panel.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mousePressed(MouseEvent e) {
                            super.mousePressed(e);

                            if (x == 0 || y == 0 || isCleared) {
                                int px = x;
                                int py = y;
                                x = e.getX();
                                y = e.getY();
                                pointsList.add(new Point(x, y));
                                isCleared = false;
                            }
                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {
                            super.mouseReleased(e);
                            System.out.println("Mouse released triangle");
                            x2 = e.getX();
                            y2 = e.getY();
                            drawPixel(x2, y2);
                            Point bod = new Point(x2, y2);
                            pointsList.add(bod);

                            //midpoint - sx, sy center of the circle, r - radius
                            int sx = (x + x2) / 2;
                            int sy = (y + y2) / 2;
                            raster.setPixel(sx, Math.round(sy), 0xff0000);

                            double r = Math.sqrt((sx - x) * (sx - x) + (sy - y) * (sy - y));

                            double tx;
                            double ty;

                            tx = sx + r * Math.sin(60 * Math.PI / 180);
                            ty = sy + r * Math.cos(120 * Math.PI / 180);

                            raster.setPixel((int) tx, (int) Math.round(ty), 0xff0000);
                            Point t = new Point((int) tx, (int) ty);
                            drawPixel((int) tx, (int) ty);
                            pointsList.add(new Point((int) tx, (int) ty));

                            filledLineRasterizer.drawLine((int) tx, (int) ty, e.getX(), e.getY());
                            filledLineRasterizer.drawLine((int) tx, (int) ty, pointsList.get(0).x, pointsList.get(0).y);

                            // TODO dorastrovat třetí stranu trojúhelníku

                            System.out.println("Circle");
                            panel.repaint();
                        }
                    });
                }
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if (panel.getWidth() < 1 || panel.getHeight() < 1)
                    return;
                if (panel.getWidth() <= raster.getWidth() && panel.getHeight() <= raster.getHeight()) //no resize if new one is smaller
                    return;
                RasterBufferedImage newRaster = new RasterBufferedImage(panel.getWidth(), panel.getHeight());

                newRaster.draw(raster);
                raster = newRaster;
                rasterizer = new LineRasterizerGraphics(raster);
            }
        });
    }

    public void clear(int color) {
        raster.setClearColor(color);
        raster.clear();
    }

    public void present(Graphics graphics) {
        raster.repaint(graphics);
    }

    public void start() {
        clear(0xaaaaaa);
        raster.getGraphics().drawString("Use mouse buttons and try resize the window", 5, 15);
        panel.repaint();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new CanvasRasterBufferedImage(800, 600).start());
    }

    public void draw() {
        clear(0x222222);
        rasterizer.rasterize(x, y, x2, y2, 0x222222);
        panel.repaint();
    }

    public void drawPixel(int a, int b) {
        int size = 2;
        int color = 0xFFFFFF;
        for (int i = -size; i <= size; i++)
            for (int j = -size; j <= size; j++)
                raster.setPixel(a + i, b + j, color);
    }
}
