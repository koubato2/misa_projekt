package rasterize;

import model.Point;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ScanLineRasterizer {
    private Raster raster;
    List<Integer> xPointsOnY = new ArrayList<>();
    private LineRasterizer trivialLineRasterizer;

    public ScanLineRasterizer(Raster rasterizer) {
        raster = rasterizer;
        trivialLineRasterizer = new TrivialLineRasterizer(raster);
        //       int i = 0;
//        int j = 0;
    }

    public void fill() {
        System.out.println("Fill begin");
        for (int i = 1; i < raster.getHeight()-1; i++) { //projizdi y
            for (int j = 1; j < raster.getWidth()-1; j++) { //projizdi x
                if (raster.getPixel(j, i) == Color.YELLOW.getRGB()) {
                    if(raster.getPixel(j+1, i) != Color.YELLOW.getRGB()) xPointsOnY.add(j);
                }
            }
            if (xPointsOnY.size()%2 == 0 && xPointsOnY.size()!=0) { //now for just two lines on the same height
                for(int n = 0; n < xPointsOnY.size(); n = n+2){
                    trivialLineRasterizer.rasterize(xPointsOnY.get(n), i, xPointsOnY.get(n+1), i, 0xFFFFFA);
                    System.out.println("Drawing Line!");
                }
                xPointsOnY.clear();

            }
            else{
                xPointsOnY.clear();
            }

        }
    }
}



